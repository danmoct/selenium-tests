package com.Selenium.Selenium_Test;


import com.Selenium.Selenium_Test.test.*;

/**
 * Hello world!
 *
 */
public class WebTest 
{
    public static void main ( String[] args )
    {
    	try
    	{
    		Test test = new ESPN_Test();
    		test.go();
    		System.out.println("Testing Complete");
    	}
    	catch(Exception e)
    	{
    		System.out.println("Couldn't Run Test.  Ending Application");
    	}
    	
    }
}
