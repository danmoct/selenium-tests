package com.Selenium.Selenium_Test.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Page {
	public static WebDriver driver;
	
		
	public Page(String URL)
	{
		System.setProperty("webdriver.chrome.driver",
	            "C:\\Users\\danielm\\Downloads\\chromedriver_win32\\chromedriver.exe");
		this.driver = new ChromeDriver();
		driver.get(URL);
		driver.manage().window().maximize();
	}	
	
}
