package com.Selenium.Selenium_Test.test;

import com.Selenium.Selenium_Test.page.Page;

public class Amazon_Test extends Test {
	
	private final String URL = "http://www.amazon.com";
	
	public void go() throws InterruptedException 
	{
		Page test = new Page(URL);		
		search_click("id","nav-link-accountList");
		search_sendkeys("id", "ap_email", "AmazonTest1234567890@gmail.com" );
		search_click("id","continue");
		search_sendkeys("id","ap_password","Password1234!");
		search_click("id","signInSubmit");
		search_sendkeys("id","twotabsearchtextbox","Headphones");
		search_sendNative("id","twotabsearchtextbox","Keys.ENTER");
		search_click("xpath","//*[contains(text(),'Vogek On Ear Headphones with Mic,')]");
		search_click("id","add-to-cart-button");
		search_click("id","nav-cart");
		search_click("id","a-autoid-0-announce");
		search_click("cssSelector","a[id *= 'dropdown1_4']");
				
	}		
	
}
