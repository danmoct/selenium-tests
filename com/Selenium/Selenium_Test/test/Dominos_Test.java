package com.Selenium.Selenium_Test.test;



import com.Selenium.Selenium_Test.page.Page;

public class Dominos_Test extends Test{
private final String URL = "http://www.dominos.com";
	
	public void go()  throws InterruptedException
	{
		Page test = new Page(URL);	
		
		search_click("className","js-close-button");
		
		
		search_click("linkText", "CARRYOUT");
		
		search_sendkeys("id", "Postal_Code_Sep", "60077");
		
		search_sendNative("id", "Postal_Code_Sep", "Keys.ENTER");
		
		search_click("className", "location-search-result__btn");
		
		search_click("className", "media__title");
		
		search_click("id", "crust_type|14BK");
		
		scroll("genericOverlay", 0, 200);
		
		search_click("className", "sizes-wrapper__size-code--16");
		
		search_click("className", "js-next");
		
		search_click("name", "Weight|X");
		
		search_sendNative("name", "Weight|X", "Keys.ARROW_DOWN");
		
		search_sendNative("name", "Weight|X", "Keys.ENTER");
		
		search_click("className", "js-next");
		search_click("className", "btn--no-thanks");
		search_click("className", "topping-Du");
		
		search_click("xpath", "//*[@id=\"toppingsWrapper\"]/div/div/div[1]/div/div/div[2]/div[2]/div/div/div[1]");
		
		search_click("className", "topping-P");
		
		search_click("xpath", "//*[@id=\"toppingsWrapper\"]/div/div/div[1]/div/div/div[1]/div[4]/div/div/div[3]");
		
		search_click("className", "topping-Ht");
		
		search_click("className", "topping-G");
		
		search_click("className", "js-next");
		scroll("genericOverlay", 0, 500);
		
		search_click("className", "js-addPizzaOnly");
		
		search_click("linkText", "CHECKOUT");
		
		search_click("className", "js-closeButton");
		
		search_click("linkText", "CONTINUE CHECKOUT");
				
	}

}
