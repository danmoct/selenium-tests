package com.Selenium.Selenium_Test.test;

import com.Selenium.Selenium_Test.page.Page;

public class Dunkin_Test extends Test{
	private String URL = "http://dunkindonuts.com";
	
	public void go() throws InterruptedException
	{
		Page test = new Page(URL);
		
		
		
	
		
		search_click("linkText", "VIEW MENU");
		
		search_click("xpath", "//*[contains(text(), 'DONUTS')]");
		
		search_sendNative("cssSelector", "a[href*='donuts/donuts']", "Keys.ENTER");
		scroll(0,500);
		search_click("id", "select2-DRP_FLAVOR-container");
		search_click("cssSelector", "li[id*= 'Cherry Blossom Donut']");
		int calories = Integer.parseInt(search_getText("cssSelector", "span[data-key *= '{calories}']"));
		int sugar = Integer.parseInt(search_getText("cssSelector","span[data-key *= '{sugarsTotal}']"));
		search_click("id", "select2-DRP_FLAVOR-container");
		search_click("cssSelector", "li[id*= 'Flyers Donut']");
		calories += Integer.parseInt(search_getText("cssSelector", "span[data-key *= '{calories}']"));
		sugar += Integer.parseInt(search_getText("cssSelector","span[data-key *= '{sugarsTotal}']"));
		System.out.println("Eating a Cherry Blossom Donut and Flyers Donut would be " + calories + " calories and " + sugar + " grams of sugar." );
	}

}
