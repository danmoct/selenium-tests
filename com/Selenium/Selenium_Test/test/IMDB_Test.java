package com.Selenium.Selenium_Test.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Selenium.Selenium_Test.page.Page;


public class IMDB_Test extends Test {
	private final String URL = "http://www.imdb.com";
	
	public void go() throws InterruptedException
	{
		Page test = new Page(URL);		
		search_click("xpath","//*[@id=\"imdbHeader\"]/div[2]/div[6]/a");
		search_click("xpath","//*[@id=\"signin-options\"]/div/div[1]/a[1]/span[2]");
		search_sendkeys("xpath", "//*[@id=\"ap_email\"]", "bqn37467@bcaoo.com" );
		search_sendkeys("xpath", "//*[@id=\"ap_password\"]", "Qwerty123$" );
		search_click("xpath","//*[@id=\"signInSubmit\"]");	
		search_sendkeys("name", "q", "Knives Out" );
		search_sendNative("name","q","Keys.ENTER");
		search_click("linkText","Knives Out");			
		search_click("xpath","//*[@id=\"title-overview-widget\"]/div[1]/div[2]/div/div[2]/div[1]/div[1]/div");
		search_click("xpath","//*[@id=\"imdbHeader\"]/div[2]/div[5]/a/div");
				
	}
}