package com.Selenium.Selenium_Test.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.Selenium.Selenium_Test.page.Page;

public class Test {
	
	//protected Page driver;

	public void go() throws InterruptedException {
		
		// TODO Auto-generated method stub
		
	}
	
	protected void search_click (String type, String path) throws InterruptedException
	{
		WebElement element = search(type,path);
		try
		{		
			element.click();
		}
		catch(Exception e)
		{
			System.out.println("Waiting for Click");
			TimeUnit.SECONDS.sleep(5);
			System.out.println("Trying to click");
			element.click();
			System.out.println("here");
		}
	}
	
	protected String search_getText(String type, String path) throws InterruptedException
	{
		String text = "";
		WebElement element = search(type,path);
		try
		{		
			text = element.getText();
			System.out.println("got attribute");
		}
		catch(Exception e)
		{
			System.out.println("Waiting for getText");
			TimeUnit.SECONDS.sleep(5);
			System.out.println("Trying to getText");
			text = element.getText();
			System.out.println("here");
		}
		
		return text;
		
	}
	
	protected void search_hover(String type, String path)
    {
        WebElement element = search(type,path);
        Actions action = new Actions(Page.driver);
        action.moveToElement(element).perform();
    }
	
	protected void search_sendkeys(String type, String path, String keys) throws InterruptedException
	{
		WebElement element = search(type,path);
		try
		{
			element.sendKeys(keys);
		}
		catch(Exception e)
		{
			System.out.println("Waiting to send Text");
			TimeUnit.SECONDS.sleep(5);
			element.sendKeys(keys);
		}
	}
	
	protected void search_sendNative(String type, String path, String keys) throws InterruptedException
	{
		WebElement element = search(type,path);
		try
		{
			switch (keys)
			{
				case "Keys.ENTER":
					element.sendKeys(Keys.ENTER);
					break;
				case "Keys.ARROW_DOWN":
					element.sendKeys(Keys.ARROW_DOWN);
					break;
			}
		}
		catch(Exception e)
		{
			switch (keys)
			{
				case "Keys.ENTER":
					element.sendKeys(Keys.ENTER);
					break;
				case "Keys.ARROW_DOWN":
					element.sendKeys(Keys.ARROW_DOWN);
					break;
			}
		}
	}
	
	private WebElement search(String type, String path)
	{
		WebElement element;
		WebDriverWait wait = new WebDriverWait(Page.driver, 10);
		
		switch (type)
		{
		case "id":
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(path)));		
			return element;
			
		case "xpath":
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(path)));			
			return element;
			
		case "linkText":
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(path)));	
			System.out.println("found" + path);
			return element;
			
		case "cssSelector":
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(path)));	
			System.out.println("found " + path);
			return element;
			
		case "name":
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(path)));			
			return element;
		case "className":
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(path)));			
			return element;
			
		}
		
		return null;		
		
	}
	
	protected void scroll(String path, int x, int y) 
	{
		JavascriptExecutor js = (JavascriptExecutor) Page.driver;
		js.executeScript(path + ".scrollBy(" + x + "," + y +")");
	}
	
	protected void scroll(int x, int y) 
	{
		JavascriptExecutor js = (JavascriptExecutor) Page.driver;
		js.executeScript("scrollBy(" + x + "," + y +")");
	}
	
	
}
